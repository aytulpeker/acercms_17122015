﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AcerCMS.Models
{
    [MetadataType(typeof(NewsMetadata))]
    public partial class News
    {       
    }

    class NewsMetadata
    {

        public int Id { get; set; }        
        [Display(Name="Başlık")]
        public string Title { get; set; }
        public string ShortDesctription { get; set; }
        public string Content { get; set; }
        public System.DateTime UpdateDate { get; set; }
        public bool IsDeleted { get; set; }
        public int ViewCount { get; set; }
        public Nullable<int> PictureId { get; set; }

        public virtual Picture Picture { get; set; }
    }

    public enum EntityType
    {
        News,
        Post
    }
}